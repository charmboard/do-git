from pymongo import MongoClient
from bson.objectid import ObjectId

uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
#uri = "mongodb://139.59.83.215:27017/charmboard_20180607" #change
client = MongoClient(uri)
db = client.get_database()
#coll = db.card_noval
coll = db.card

required_list = [ "additional_attributes", "associated_charms", "card_click_count", "card_id", "card_like_count", "card_save_count", "card_type", "card_view_count", "circle_flag", "moreimages",  "product_url", "shoppable", "subtitle", "title",]# "main_category", "category", "sub_category", "subsub_category",]

allowed_list = ["_id", "actionfunction1", "actionfunction2", "actionlabel1", "actionlabel2", "actionname1", "actionname2", "additional_attributes", "associated_charms", "campaign_id", "card_action", "card_click_count", "card_id", "card_like_count", "card_save_count", "card_size", "card_type", "card_view_count", "cardorder", "carousel", "category", "circle_flag", "color_name", "created_on", "description", "dominant_color", "heading", "keywords", "live_status", "main_category", "modified_on", "moreimages", "product_url", "protip", "shoppable", "slider", "sub_category", "subsub_category", "subtitle", "st_num", "title", "vendor_data"]

for card in coll.find({ "_id" : { "$gt" : ObjectId("5b3221d61f6fda04644bf330") }}):
	keys_list = card.keys()
	for req_field in required_list:
		if req_field not in keys_list:
			print(card['card_id'],req_field,"missing")
	for key in keys_list:
		if key not in allowed_list:
			print(card['card_id'],repr(key),"is not allowed!!")
		else:
			if key == "_id":
				continue
			elif key in [ "actionfunction1" , "actionfunction2" ] :
				if card[key] not in ([ "appendsimilar", "appendsimilar_apparel", "appendslider", "openimage", "opennewpage", "playvideo", "sharecontent", "" ]):
					print(card['card_id'],key,type(card[key]).__name__,"datatype mismatch")
			elif key in ["actionlabel1", "actionlabel2"]:
				if card[key] not in ([ "Buy It", "Shop", "Read More", "Explore", "Test Drive", "Play", "Watch", "Share", "Learn More", "Trip Advisor", "Close Steps", "View Similar", "-1", "", "null"]):
					print(card['card_id'],key,card[key],type(card[key]).__name__,"incorrect value")
			elif key in ["actionname1" , "actionname2" ] :
				if card[key] not in (["A11", "A21", "A31", "A41", "A111", "A42", "A43", "A51", "A52", "A61", "A71", "A81", "A91", "A101", "-1", "", "null" ]):
					print(card['card_id'],key,card[key],type(card[key]).__name__,"incorrect value")
			elif key in ["campaign_id", "card_size" ,"dominant_color" ,"heading" , "moreimages" , "product_url" , "sub_category" , "subsub_category" , "st_num"]:
				if type(card[key]).__name__ != "str":
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key == "card_action" or  key == "card_click_count" or  key == "card_id" or  key == "card_like_count" or  key == "card_save_count" or  key == "card_view_count":
				if type(card[key]).__name__ != "int":
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key == "main_category":
				if card[key] not in (["women", "men", ]):
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "shoppable":
				if card[key] not in (["referal", ""]):
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "card_type":
				if card[key] in ([ "-1", "ITA" ]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "cardorder":
				if card[key] in ([ "ITA", "IDA", "ID", "IA", "", "-1", "TIA", "TSIDA", "I", "TSIA", "TSID", "TSI" ]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "category":
				if card[key] in ([ "outfit", "accessories", "beauty", "hair", "lingerie", "-1", "Outfit", "Accessories", "Beauty", "Hair", "" ]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "color_name":
				if card[key] in (["assorted", "beige", "beige pink", "beige purple", "black", "blue", "bronze", "brown", "burgundy", "camel", "camel brown", "champagne", "charcoal", "coffee brown", "cognac", "copper", "coral", "cream", "fluorescent", "fluorescent green", "fuchsia", "gold", "gold silver", "green", "grey", "grey melange", "gunmetal", "khaki", "lavender", "lime green", "magenta", "maroon", "mauve", "melange", "metallic", "multi", "mushroom", "mushroom brown", "mustard", "navy", "navy blue", "nude", "off white", "olive", "orange", "peach", "peach cream", "pink", "purple", "red", "red grey", "rose", "rose gold", "rust", "sea green", "silver", "skin", "steel", "tan", "taupe", "teal", "transparent", "turquoise", "violet", "white", "yellow", "others", None,]):# "light", "ligth", "-1", "brown family", "white family", "light blue", "light brown", "light orange", "none", "ligth brown"]):
					continue
				else:
					print(card['card_id'],key,repr(card[key]),type(card[key]).__name__,"incorrect value")
			elif key in ["circle_flag", "live_status", "protip", "slider"]:
				if type(card[key]).__name__ != 'int':
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
				elif card[key] not in [0,1,-1]:
					print(card['card_id'],type(card[key]).__name__,key,"unacceptable value")
				elif card[key] == -1 and key != 'slider':
					print(card['card_id'],key,"-1 not allowed")
			elif key in ["created_on", "modified_on", ]:
				if type(card[key]).__name__ in ["Int64", "float"]:
					continue
				else:
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key in ["carousel"]:
				if type(card[key]).__name__ == "list":
					continue
				else:
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key == 'associated_charms':
				charms = card[key]
				for charm in charms:
					try:
						inner_keys = charm.keys()
						for inner_key in inner_keys:
							if inner_key not in [ "charm_id"]:
								print(card['card_id'],key,repr(inner_key),"not allowed")
							else:
								if inner_key == 'charm_id':
									if type(charm[inner_key]).__name__ not in ['int', 'float']:
										print(card['card_id'],key,inner_key,type(charm[inner_key]).__name__,charm[inner_key],"datatype mismatch")
					except AttributeError:
						print(card['card_id'],key,card[key],"empty list")
			elif key in ['title','subtitle','description']:
				inner_keys = card[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['en','en-gb']:
						print(card['card_id'],key,repr(inner_key),"not allowed")
					elif type(card[key][inner_key]).__name__ != 'str':
						print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
			elif key == "keywords":
				keywords = card[key]
				for keyword in keywords:
					inner_keys = keyword.keys()
					for inner_key in inner_keys:
						if inner_key not in ['subtitle','title','video_name']:
							print(card['card_id'],key,repr(inner_key),"not allowed")
						elif type(keyword[inner_key]).__name__ != 'str':
							print(card['card_id'],key,inner_key,type(keyword[inner_key]).__name__,"datatype mismatch")
			elif key == "vendor_data":
				inner_keys = card[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['actual_price','sale_price','product_id','disc_perc']:
						print(card['card_id'],key,repr(inner_key),"not allowed")
					elif inner_key == 'product_id':
						if type(card[key][inner_key]).__name__ != 'str':
							print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
					elif type(card[key][inner_key]).__name__ not in ['int', 'float']:
						print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
					elif inner_key == 'disc_perc': 
						if 0 <= card[key][inner_key] < 100:
							continue
						else:
							print(card['card_id'],key,inner_key,card[key][inner_key],"not within 0 to 100")
			elif key == "additional_attributes":
				try:
					inner_keys = card[key].keys()
					for inner_key in inner_keys:
						if inner_key not in ['advertiser_brand','product_brand']:
							print(card['card_id'],key,repr(inner_key),"not allowed")
						elif type(card[key][inner_key]).__name__ != 'str':
							print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
				except AttributeError:
					print(card['card_id'],key,card[key],"should not be an array")
			else:
				print(card['card_id'],key,type(card[key]).__name__)
