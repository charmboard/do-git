from pymongo import MongoClient
from bson.objectid import ObjectId

#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a1.mlab.com:19295/charmboard"
#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
uri = "mongodb://cbappprod:cbapprod#132@cb-app-2.charmd.me:19295/charmboard"
#uri = "mongodb://139.59.83.215:27017/charmboard_20180607" #change
client = MongoClient(uri)
db = client.get_database()
#db = client.charmboard_20180607 #change
#coll = db.board_noval
coll = db.board

required_list = [ "board_card_count", "board_cat", "board_charm_count", "board_description", "board_id", "board_name", "board_open_option", "board_type", "board_video_count", "board_vis", "created_on", "lang", "modified_on", "user_id", ]

allowed_list = ["_id", "bimg", "board_card_count", "board_cat", "board_charm_count", "board_description", "board_id", "board_name", "board_open_option", "board_ranking", "board_type", "board_video_count", "board_vis", "created_on", "deleted", "fscore", "lang", "modified_on", "user_id"]

for board in coll.find(): #{ "_id" : { "$gt" : ObjectId("5b12dc815e111aa41cfdca54") }}):
	keys_list = board.keys()
	for req_field in required_list:
		if req_field not in keys_list:
			print(board['board_id'],req_field,"missing")
	for key in keys_list:
		if key not in allowed_list:
			print(board['board_id'],key,"is not allowed")
		else:
			if key == "_id":
				continue
			elif key == "board_cat":
				if board[key] in ([ "jewellery", "footwear", "wfashion", "hairbeauty", "makeup", "Clothes", "actors", "", "mfashion", "character", "travel", "video", "weddings", "facc", "mensgrooming", "topcharms", "beauty", "bags", "innerwear" ]):
					continue
				else:
					print(board['board_id'],key,board[key],"incorrect value")
			elif key == "board_open_option":
				if board[key] in (["charms", "cards"]):
					continue
				else:
					print(board['board_id'],key,board[key],"incorrect value")
			elif key == "board_type":
				if board[key] in ([ "custom", "default", "system" ]):
					continue
				else:
					print(board['board_id'],key,board[key],"incorrect value")
			elif key in ["deleted"]:
				if type(board[key]).__name__ != 'int':
					print(board['board_id'],type(board[key]).__name__,key,"datatype mismatch")
				elif board[key] not in [0,1]:
					print(board['board_id'],type(board[key]).__name__,key,"unacceptable value")
			elif key in ["created_on", "modified_on", ]:
				if type(board[key]).__name__ in ["Int64", "float"]:
					continue
				else:
					print(board['board_id'],type(board[key]).__name__,key,"datatype mismatch")
			elif key in ["fscore", ]:
				if type(board[key]).__name__ in ["int", "float"]:
					continue
				else:
					print(board['board_id'],type(board[key]).__name__,key,"datatype mismatch")
			elif key in ["user_id", ]:
				if type(board[key]).__name__ in ["int", "float", "Int64"]:
					continue
				else:
					print(board['board_id'],type(board[key]).__name__,key,"datatype mismatch")
			elif key in ["bimg",]:
				if type(board[key]).__name__ == "str":
					continue
				else:
					print(board['board_id'],type(board[key]).__name__,key,"datatype mismatch")
			elif key in ["lang",]:
				if type(board[key]).__name__ == "list":
					continue
				else:
					print(board['board_id'],type(board[key]).__name__,key,"datatype mismatch")
			elif key in ['board_description','board_name']:
				inner_keys = board[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['en','en-gb']:
						print(board['board_id'],key,inner_key,"not allowed")
			elif key in ['board_card_count','board_charm_count','board_id','board_ranking','board_vis','board_video_count']:
				if type(board[key]).__name__ == 'int':
					continue
				else:
					print(board['board_id'],type(board[key]).__name__,key,"datatype mismatch")
			else:
				print(board['board_id'],key,type(board[key]).__name__)

