from pymongo import MongoClient
from bson.objectid import ObjectId

#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
#uri = "mongodb://139.59.83.215:27017/charmboard_20180607" #change
client = MongoClient(uri)
db = client.get_database()
#db = client.charmboard_20180607 #change
#coll = db.charm_noval
coll = db.charm

required_list = [ "aux_charm_flag", "charm_id", "charm_like_count", "charm_save_count", "charm_touch_count", "charm_view_count", "chsketch_id", "created_on", "live_status", "look", "modified_on", "objects_in_charm", "subtitle", "tagger_object", "title", ]

allowed_list = ["_id", "actor_id", "associated_videos", "aux_charm_flag", "board_obj", "character_id", "charm_id", "charm_like_count", "charm_save_count", "charm_touch_count", "charm_view_count", "chsketch_id", "content_image_cards", "created_on", "ext_likes", "fscore", "isGif", "lang", "lc_bounding_box", "live_status", "look", "modified_on", "objects_in_charm", "production_name", "qscore", "ref_cards", "search_desc", "search_keywords", "subtitle", "tagger_object", "title", "video_id",  ]

for charm in coll.find(): #{ "_id" : { "$gt" : ObjectId("5b129e551f6fda4376a70762") }}):
	keys_list = charm.keys()
	for req_field in required_list:
		if req_field not in keys_list:
			print(charm['charm_id'],req_field,"missing")
	for key in keys_list:
		if key not in allowed_list:
			print(charm['charm_id'],key,"is not allowed")
		else:
			if key == "_id":
				continue
			elif key in ["lc_bounding_box", "production_name", "tagger_object", "video_id"]:
				if type(charm[key]).__name__ == "str":
					continue
				else:
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ['charm_id','charm_like_count','charm_save_count','charm_touch_count','charm_view_count']:
				if type(charm[key]).__name__ != 'int':
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ["aux_charm_flag", "live_status", "isGif", "look"]:
				if type(charm[key]).__name__ != 'int':
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
				elif charm[key] not in [0,1]:
					print(charm['charm_id'],type(charm[key]).__name__,key,"unacceptable value")
			elif key in ["created_on", "modified_on", ]:
				if type(charm[key]).__name__ not in ["Int64", "float"]:
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ["actor_id", "character_id", "chsketch_id", "fscore" , "qscore" ]:
				if type(charm[key]).__name__ in ["int", "float"]:
					continue
				else:
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ["ext_likes", "lang", "objects_in_charm", "search_desc", "search_keywords"]:
				if type(charm[key]).__name__ == "list":
					continue
				else:
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			elif key in ['title','subtitle',]:
				inner_keys = charm[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['en','en-gb']:
						print(charm['charm_id'],key,repr(inner_key),"not allowed")
					elif type(charm[key][inner_key]).__name__ != 'str':
						print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
			elif key == 'associated_videos':
				videos = charm[key]
				try:
					for video in videos:
						inner_keys = video.keys()
						reqd_inner_keys = [ "video_url", "video_id", "video_type", "video_category", "video_year", "video_name",]
						for reqd_inner_key in reqd_inner_keys:
							if reqd_inner_key not in inner_keys:
								print(charm['charm_id'],inner_key,"missing")
						for inner_key in inner_keys:
							if inner_key not in ['band_name', 'cueid', 'movie_name', 'video_url', 'song_name', 'video_id', 'video_type', 'video_category', 'video_year', 'video_name',]:
								print(charm['charm_id'],key,repr(inner_key),"not allowed")
							else:
								if inner_key in ['band_name','movie_name','video_url','song_name','video_id','video_name',]:
									if type(video[inner_key]).__name__ != 'str':
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key in ['cueid','video_year']:
									if type(video[inner_key]).__name__ not in ['int','float']:
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,video[inner_key], "datatype mismatch")
								elif inner_key == 'video_type':
									if video[inner_key] not in [ "Movie", "Television/Serial", "Ads", "Web Series" ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
								elif inner_key == 'video_category':
									if video[inner_key] not in [ "Trailer", "Song", "Episode", "Ads", "Web Series" ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
				except TypeError:
					print(charm['charm_id'],key,"empty list", charm[key])
			elif key == 'board_obj':
				videos = charm[key]
				for video in videos:
					inner_keys = video.keys()
					for inner_key in inner_keys:
						if inner_key not in ["board_id", "video_id"]:
							print(charm['charm_id'],inner_key,"missing")
							continue
						if inner_key not in ["board_id", "video_id"]:
							print(charm['charm_id'],key,repr(inner_key),"not allowed")
						else:
							if inner_key in ['video_id',]:
								if type(video[inner_key]).__name__ != 'str':
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
							elif inner_key in ['board_id']:
								if type(video[inner_key]).__name__ not in ['int',]:
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
			elif key == 'content_image_cards':
				videos = charm[key]
				try:
					for video in videos:
						inner_keys = video.keys()
						for inner_key in inner_keys:
							if inner_key not in ["card_color", "card_id",'dis_card_type', 'card_rank', 'cb_category', 'sub_category', 'subsub_category', 'text',]:
								print(charm['charm_id'],key,repr(inner_key),"not allowed")
							else:
								if inner_key in ["card_color", 'dis_card_type', 'sub_category', 'subsub_category', 'text',]:
									if type(video[inner_key]).__name__ != 'str':
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key in ['card_id', ]:
									if type(video[inner_key]).__name__ not in ['int','str', 'float']:
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key in ['card_id', 'card_rank']:
									if type(video[inner_key]).__name__ not in ['int','str']:
										print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
								elif inner_key == 'cb_category':
									if video[inner_key] not in [ "outfit",  "beauty", "accessories", "hair", "-1" ]:
										print(charm['charm_id'],key,inner_key,video[inner_key],"incorrect value")
				except TypeError:
					print(charm['charm_id'],key,"empty list",charm[key])
			elif key == 'ref_cards':
				videos = charm[key]
				for video in videos:
					inner_keys = video.keys()
					for inner_key in inner_keys:
						if inner_key not in ["card_id", "category",'product_url','title','type']:
							print(charm['charm_id'],key,repr(inner_key),"not allowed")
						else:
							if inner_key in ["category",'product_url','title','type']:
								if type(video[inner_key]).__name__ != 'str':
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
							elif inner_key in ['card_id']:
								if type(video[inner_key]).__name__ not in ['int','float', 'str']:
									print(charm['charm_id'],key,inner_key,type(video[inner_key]).__name__,"datatype mismatch")
			elif key == "additional_attributes":
				if type(charm[key]).__name__ != 'dict':
					print(charm['charm_id'],type(charm[key]).__name__,key,"datatype mismatch")
			else:
				print(charm['charm_id'],key,type(charm[key]).__name__)
