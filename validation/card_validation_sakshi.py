from pymongo import MongoClient

#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
#uri = "mongodb://139.59.83.215:27017/gcp_clean" #change
client = MongoClient(uri)
db = client.get_database()
#coll = db.card_noval
coll = db.card

required_list = [ "additional_attributes", "associated_charms", "card_click_count", "card_id", "card_like_count", "card_save_count", "card_type", "card_view_count", "circle_flag", "moreimages",  "product_url", "shoppable", "subtitle", "title",]# "main_category", "category", "sub_category", "subsub_category",]

allowed_list = ["_id", "actionfunction1", "actionfunction2", "actionlabel1", "actionlabel2", "actionname1", "actionname2", "additional_attributes", "associated_charms", "campaign_id", "card_action", "card_click_count", "card_id", "card_like_count", "card_save_count", "card_size", "card_type", "card_view_count", "cardorder", "carousel", "category", "circle_flag", "color_name", "created_on", "description", "dominant_color", "heading", "keywords", "live_status", "main_category", "modified_on", "moreimages", "product_url", "protip", "shoppable", "slider", "sub_category", "subsub_category", "subtitle", "st_num", "title", "vendor_data"]

for card in coll.find():
	keys_list = card.keys()
	for req_field in required_list:
		if req_field not in keys_list:
			print(card['card_id'],req_field,"missing")
	for key in keys_list:
		if key not in allowed_list:
			print(card['card_id'],repr(key),"is not allowed!!")
		else:
			if key == "_id":
				continue
			elif key == "actionfunction1":
				if card[key] in ([ "opennewpage", "", "appendsimilar", "appendslider", "appendsimilar_apparel", "playvideo"]):
					continue
				else:
					print(card['card_id'],key,"datatype mismatch")
			elif key == "actionfunction2":
				if card[key] in ([ "opennewpage", "", "appendslider", "appendsimilar_apparel", "playvideo"]):
					continue
				else:
					print(card['card_id'],key,"incorrect value")
			elif key == "actionlabel1":
				if card[key] in ([ "Buy It", "-1", "Close Steps", "Shop", "", "Explore", "Learn More", "View Similar", "Watch", "Similar", "Play", "Trip Advisor"]):
					continue
				else:
					print(card['card_id'],key,"incorrect value")
			elif key == "actionlabel2":
				if card[key] in ([ "", "null", "Learn More", "View Similar", "Watch", "Explore", "Shop", "Buy It"]):
					continue
				else:
					print(card['card_id'],key,"incorrect value")
			elif key == "actionname1":
				if card[key] in (["A11", "-1", "A91", "A21", "A41", "A71", "A101", "", "A52", "A42", "A51", "A81" ]):
					continue
				else:
					print(card['card_id'],key,"incorrect value")
			elif key == "actionname2":
				if card[key] in ([ "", "-1", "null", "A71", "A101", "A52", "A42", "A11", "A21", "A41" ]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key in ["campaign_id", "card_size" ,"dominant_color" ,"heading" , "moreimages" , "product_url" , "sub_category" , "subsub_category" , "st_num"]:
				if type(card[key]).__name__ == "str":
					continue
				else:
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key == "card_action" or  key == "card_click_count" or  key == "card_id" or  key == "card_like_count" or  key == "card_save_count" or  key == "card_view_count":
				if type(card[key]).__name__ == "int":
					continue
				else:
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key == "main_category":
				if card[key] in (["women", "men", "female", "male", "Women", "Men", "Female", "Male", ""]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "shoppable":
				if card[key] in (["referal", ""]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "card_type":
				if card[key] in ([ "-1", "ITA" ]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "cardorder":
				if card[key] in ([ "ITA", "IDA", "ID", "IA", "", "-1", "TIA", "TSIDA", "I", "TSIA", "TSID", "TSI" ]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "category":
				if card[key] in ([ "outfit", "accessories", "beauty", "hair", "lingerie", "-1", "Outfit", "Accessories", "Beauty", "Hair", "" ]):
					continue
				else:
					print(card['card_id'],key,card[key],"incorrect value")
			elif key == "color_name":
				if card[key] in (["Assorted", "Beige", "Beige Pink", "Beige Purple", "Black", "Blue", "Bronze", "Brown", "Burgundy", "Camel", "Camel Brown", "Champagne", "Charcoal", "Coffee Brown", "Cognac", "Copper", "Coral", "Cream", "Fluorescent", "Fluorescent Green", "Fuchsia", "Gold", "Gold Silver", "Green", "Grey", "Grey Melange", "Gunmetal", "Khaki", "Lavender", "Lime Green", "Magenta", "Maroon", "Mauve", "Melange", "Metallic", "Multi", "Mushroom", "Mushroom Brown", "Mustard", "Navy", "Navy Blue", "Nude", "Off White", "Olive", "Orange", "Peach", "Peach Cream", "Pink", "Purple", "Red", "Red Grey", "Rose", "Rose Gold", "Rust", "Sea Green", "Silver", "Skin", "Steel", "Tan", "Taupe", "Teal", "Transparent", "Turquoise", "Violet", "White", "Yellow", "Others", "Light", "Ligth", "-1", "Brown Family", "White Family", "Light Blue", "Light Brown", "Light Orange", "None", "Ligth Brown", None]):
					continue
				else:
					print(card['card_id'],key,repr(card[key]),type(card[key]).__name__,"incorrect value")
			elif key in ["circle_flag", "live_status", "protip", "slider"]:
				if type(card[key]).__name__ != 'int':
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
				elif card[key] not in [0,1,-1]:
					print(card['card_id'],type(card[key]).__name__,key,"unacceptable value")
				elif card[key] == -1 and key != 'slider':
					print(card['card_id'],key,"-1 not allowed")
			elif key in ["created_on", "modified_on", ]:
				if type(card[key]).__name__ in ["Int64", "float"]:
					continue
				else:
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key in ["carousel"]:
				if type(card[key]).__name__ == "list":
					continue
				else:
					print(card['card_id'],type(card[key]).__name__,key,"datatype mismatch")
			elif key == 'associated_charms':
				charms = card[key]
				for charm in charms:
					try:
						inner_keys = charm.keys()
						for inner_key in inner_keys:
							if inner_key not in [ "charm_id"]:
								print(card['card_id'],key,repr(inner_key),"not allowed")
							else:
								if inner_key == 'charm_id':
									if type(charm[inner_key]).__name__ != 'int':
										print(card['card_id'],key,inner_key,type(charm[inner_key]).__name__,charm[inner_key],"datatype mismatch")
					except AttributeError:
						print(card['card_id'],key,card[key],"empty list")
			elif key in ['title','subtitle','description']:
				inner_keys = card[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['en','en-gb']:
						print(card['card_id'],key,repr(inner_key),"not allowed")
					elif type(card[key][inner_key]).__name__ != 'str':
						print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
			elif key == "keywords":
				inner_keys = card[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['subtitle','title','video_name']:
						print(card['card_id'],key,repr(inner_key),"not allowed")
					elif type(card[key][inner_key]).__name__ != 'str':
						print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
			elif key == "vendor_data":
				inner_keys = card[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['actual_price','sale_price','product_id','disc_perc']:
						print(card['card_id'],key,repr(inner_key),"not allowed")
					elif inner_key == 'product_id':
						if type(card[key][inner_key]).__name__ != 'str':
							print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
					elif type(card[key][inner_key]).__name__ not in ['int', 'float']:
						print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
					elif inner_key == 'disc_perc': 
						if 0 <= card[key][inner_key] < 100:
							continue
						else:
							print(card['card_id'],key,inner_key,card[key][inner_key],"not within 0 to 100")
			elif key == "additional_attributes":
				inner_keys = card[key].keys()
				for inner_key in inner_keys:
					if inner_key not in ['advertiser_brand','product_brand']:
						print(card['card_id'],key,repr(inner_key),"not allowed")
					elif type(card[key][inner_key]).__name__ != 'str':
						print(card['card_id'],key,inner_key,type(card[key][inner_key]).__name__,"datatype mismatch")
			else:
				print(card['card_id'],key,type(card[key]).__name__)
