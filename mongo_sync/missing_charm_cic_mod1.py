from pymongo import MongoClient
import pandas as pd
df = pd.read_csv('../gcp_charms.csv')
df1 = pd.read_csv('../mlab_charms.csv')


import json
import bson

uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
#uri = "mongodb://139.59.83.215:27017/charmboard_20180607" #change
client = MongoClient(uri)
db = client.get_database()
#coll = db.charm_clean
coll = db.charm


count = 0
count_missing = 0

for each in df1.charm_id:
	mlab_cic = df1.loc[df1.charm_id ==each].iloc[0].content_image_cards
	mlab_cic = json.loads(mlab_cic)
#	print(mlab_cic, len(mlab_cic))
	try:
		gcp_charm = coll.find_one({"charm_id" : int(each)})
		num = len(gcp_charm)
#		print(num, gcp_charm)
		try:
			gcp_cic = gcp_charm['content_image_cards']
		except KeyError:
			print("updating",each,mlab_cic)
			count_missing += 1
			result = coll.update_one({"charm_id": int(each)},{ "$set": { "content_image_cards" :  mlab_cic } }, upsert=False)
		if num == 1 :
			if len(mlab_cic) > 0 and len(gcp_cic) == 0:
				print(each, "Modifying empty CIC")
				result = coll.update_one({"charm_id": int(each)},{ "$set": { "content_image_cards" :  mlab_cic } }, upsert=False)
				count += 1
	except bson.errors.InvalidDocument:
		print(each,"bson")
	except TypeError:
		print(each,"not present")
print(count, count_missing)
