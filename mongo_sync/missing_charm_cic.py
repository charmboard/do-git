from pymongo import MongoClient
import pandas as pd
df = pd.read_csv('../gcp_charms.csv')
df1 = pd.read_csv('../mlab_charms.csv')


import json

uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
#uri = "mongodb://139.59.83.215:27017/charmboard_20180607" #change
client = MongoClient(uri)
db = client.get_database()
#coll = db.charm_clean
coll = db.charm


count = 0

for each in df1.charm_id:
	num = df.loc[df.charm_id == each].shape[0]
	if num ==1 :
		gcp_cic = df.loc[df.charm_id == each].iloc[0].content_image_cards
		mlab_cic = df1.loc[df1.charm_id ==each].iloc[0].content_image_cards
		try:
			gcp_cic = json.loads(gcp_cic)
		except TypeError:
			gcp_cic = []
		try:
			mlab_cic = json.loads(mlab_cic)
		except AttributeError:
			mlab_cic = []
		if len(mlab_cic) > 0 and len(gcp_cic) == 0:
			print(each)
			result = coll.update_one({"charm_id": int(each)},{ "$set": { "content_image_cards" :  mlab_cic } }, upsert=False)
			count += 1
print(count)
