from pymongo import MongoClient
import pandas as pd
import json
import ast

#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
uri = "mongodb://139.59.83.215:27017/mlab_merge"
client = MongoClient(uri)
db = client.get_database()
mlab_coll = db.charm

uri = "mongodb://139.59.83.215:27017/gcp_merge"
client = MongoClient(uri)
db = client.get_database()
local_coll = db.charm

for charm in local_coll.find():
	charm_mlab = mlab_coll.find_one({"_id" : charm['_id']})
	try:
		gcp_av = charm['associated_videos']
	except KeyError:
		gcp_av = []
	try:
		mlab_av = charm_mlab['associated_videos']
	except KeyError:
		mlab_av = []
	if gcp_av != [] and gcp_av is not None:
		gcp_av_videos = [gcp_av[i]['video_id'] for i in range(len(gcp_av))]
	else:
		gcp_av_videos = []
	if mlab_av != [] and mlab_av is not None:
		mlab_av_videos = [mlab_av[i]['video_id'] for i in range(len(mlab_av))]
	else:
		mlab_av_videos = []
	if (set(mlab_av_videos).union(gcp_av_videos) != set(mlab_av_videos)):
		print(charm['charm_id'],"<<<<<")
		print("missing in gcp: " + str(list(set(mlab_av_videos) - set(gcp_av_videos))))
		print("missing in mlab: " + str(list(set(gcp_av_videos) - set(mlab_av_videos))))
	else:
		print(charm['charm_id'])
