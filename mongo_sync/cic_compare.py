from pymongo import MongoClient
import pandas as pd
import json
import ast

#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
#uri = "mongodb://localhost:27017/mlab_merge"
client = MongoClient(uri)
db = client.get_database()
mlab_coll = db.charm

#uri = "mongodb://139.59.83.215:27017/gcp_merge"
#uri = "mongodb://localhost:27017/gcp_merge"
uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
client = MongoClient(uri)
db = client.get_database()
local_coll = db.charm

for charm in local_coll.find():
	charm_mlab = mlab_coll.find_one({"_id" : charm['_id']})
	try:
		gcp_cic = charm['content_image_cards']
	except KeyError:
		gcp_cic = []
	try:
		mlab_cic = charm_mlab['content_image_cards']
	except KeyError:
		mlab_cic = []
	if gcp_cic != [] and gcp_cic is not None:
		#print("here")
		gcp_cic_cards = [str(gcp_cic[i]['card_id']) for i in range(len(gcp_cic))]
		#print(gcp_cic_cards)
	else:
		gcp_cic_cards = []
	if mlab_cic != [] and mlab_cic is not None:
		#print("here")
		mlab_cic_cards = [str(mlab_cic[i]['card_id']) for i in range(len(mlab_cic))]
		#print(mlab_cic_cards)
	else:
		mlab_cic_cards = []
	if (set(mlab_cic_cards).union(gcp_cic_cards) != set(mlab_cic_cards)):
		print(charm['charm_id'],"<<<<<<<")
		print("missing in gcp: " + str(list(set(mlab_cic_cards) - set(gcp_cic_cards))))
		print("missing in mlab: " + str(list(set(gcp_cic_cards) - set(mlab_cic_cards))))
		result = local_coll.update_one({"charm_id": charm['charm_id']},{ "$set": { "content_image_cards" :  mlab_cic } }, upsert=False)
	else:
		print(charm['charm_id'])
