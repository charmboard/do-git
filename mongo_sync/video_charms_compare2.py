from pymongo import MongoClient
import pandas as pd
import json
import ast

#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
#uri = "mongodb://139.59.83.215:27017/mlab_merge"
#uri = "mongodb://localhost:27017/mlab_merge"
client = MongoClient(uri)
db = client.get_database()
mlab_coll = db.video

uri = "mongodb://cbappprod:cbapprod#132@cb-app-0.charmd.me:19295/charmboard"
client = MongoClient(uri)
db = client.get_database()
local_coll = db.video

for video in local_coll.find():
	video_mlab = mlab_coll.find_one({"_id" : video['_id']})
	try:
		gcp_vc = video['video_charms']
	except KeyError:
		gcp_vc = []
	try:
		mlab_vc = video_mlab['video_charms']
	except KeyError:
		mlab_vc = []
	except TypeError:
		mlab_vc = []
	if gcp_vc != [] and gcp_vc is not None:
		gcp_vc_charms = [gcp_vc[i] for i in range(len(gcp_vc))]
	else:
		gcp_vc_charms = []
	if mlab_vc != [] and mlab_vc is not None:
		mlab_vc_charms = [mlab_vc[i] for i in range(len(mlab_vc))]
	else:
		mlab_vc_charms = []
	if (set(mlab_vc_charms).union(gcp_vc_charms) != set(mlab_vc_charms)):
		print(video['video_id'],"<<<<<")
		print("missing in gcp: " + str(list(set(mlab_vc_charms) - set(gcp_vc_charms))))
		print("missing in mlab: " + str(list(set(gcp_vc_charms) - set(mlab_vc_charms))))
		result = local_coll.update_one({"video_id": video['video_id']},{ "$set": { "video_charms" :  mlab_vc_charms } }, upsert=False)
	else:
		print(video['video_id'])
