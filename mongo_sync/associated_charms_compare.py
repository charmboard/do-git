from pymongo import MongoClient
import pandas as pd
import json
import ast

#uri = "mongodb://cbappdev:cbdevap#132@cb-app-0.charmd.me:19295/charmboard_dev"
#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
#uri = "mongodb://139.59.83.215:27017/mlab_merge"
uri = "mongodb://localhost:27017/mlab_merge"
client = MongoClient(uri)
db = client.get_database()
mlab_coll = db.card

uri = "mongodb://localhost:27017/gcp_merge"
client = MongoClient(uri)
db = client.get_database()
local_coll = db.card

for card in local_coll.find():
	card_mlab = mlab_coll.find_one({"_id" : card['_id']})
	try:
		gcp_ac = card['associated_charms']
	except KeyError:
		gcp_ac = []
	try:
		mlab_ac = card_mlab['associated_charms']
	except KeyError:
		mlab_ac = []
	if gcp_ac != [] and gcp_ac is not None:
		try:
			gcp_ac_charms = [str(gcp_ac[i]['charm_id']) for i in range(len(gcp_ac))]
		except KeyError:
			print("gcp KeyError",gcp_ac)
			gcp_ac_charms = []
	else:
		gcp_ac_charms = []
	if mlab_ac != [] and mlab_ac is not None:
		try:
			mlab_ac_charms = [str(mlab_ac[i]['charm_id']) for i in range(len(mlab_ac))]
		except TypeError:
			print("mlab TypeError",mlab_ac)
			mlab_ac_charms = []
	else:
		mlab_ac_charms = []
	if (set(mlab_ac_charms).union(gcp_ac_charms) != set(mlab_ac_charms)):
		print(card['card_id'],"<<<<<")
		print("missing in gcp: " + str(list(set(mlab_ac_charms) - set(gcp_ac_charms))))
		print("missing in mlab: " + str(list(set(gcp_ac_charms) - set(mlab_ac_charms))))
	else:
		print(card['card_id'])
