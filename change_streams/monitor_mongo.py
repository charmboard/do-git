from joblib import Parallel, delayed
from pymongo import MongoClient
import json
from config import MONGO_ORIG as mongo_watch
from config import MONGO_UPDATE as mongo_push
import time
import traceback
import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

MONGO_ACCESS = "mongodb://" + mongo_watch["username"] + ":" + mongo_watch["password"] + \
                        "@" + mongo_watch["host"] + ":" + mongo_watch["port"] + \
                        "/" + mongo_watch["db_name"] #+ "?replicaSet=" + mongo_watch["replica_set"]

MONGO_UPDATE = "mongodb://" + mongo_push["host"] + ":" + mongo_push["port"] + \
                        "/" + mongo_push["db_name"] + "?replicaSet=" + mongo_push["replica_set"]

def poll_stream(collection):
    try:
        client = MongoClient(MONGO_ACCESS, connect=False)
        db = client.get_database()
        coll = db[collection]
        poll_cursor = coll.watch()
        for poll_document in poll_cursor:
            #logger.info("DOC CHANGE : " + str(poll_document))
            timestamp = round(time.time()*1000)
            insert_changes(poll_document, coll,timestamp)
        poll_stream()
    except Exception as ex:
        traceback.print_exc()
        logger.error('POLL CHANGE : ' , ex)


def insert_changes(new_doc, coll,timestamp):
    try:
        insert_vals = {}
        insert_vals['timestamp'] = timestamp
        op_type = new_doc['operationType']
        insert_vals['op_type'] = op_type
        if op_type != 'invalidate':
            insert_vals['db_name'] = new_doc['ns']['db']
            insert_vals['col_name'] = new_doc['ns']['coll']
            doc_id = new_doc['documentKey']['_id']
            insert_vals['obj_id'] = doc_id
            if op_type == 'insert':
                latest_doc = coll.find_one({'_id': doc_id})
                latest_doc['_id'] = str(latest_doc['_id'])
                insert_vals['entire_doc'] = json.dumps(latest_doc)
                insert_vals['change'] = 'N/A'
            elif op_type == 'update':
                latest_doc = coll.find_one({'_id': doc_id})
                latest_doc['_id'] = str(latest_doc['_id'])
                insert_vals['entire_doc'] = json.dumps(latest_doc)
                insert_vals['change'] = json.dumps(new_doc['updateDescription'])
            elif op_type == 'delete':
                insert_vals['entire_doc'] = 'N/A'
                insert_vals['change'] = 'N/A'
            elif op_type == 'replace':
                latest_doc = coll.find_one({'_id': doc_id})
                latest_doc['_id'] = str(latest_doc['_id'])
                insert_vals['entire_doc'] = json.dumps(latest_doc)
                insert_vals['change'] = 'N/A'
        push_to_mongo(insert_vals)
    except Exception as ex:
        traceback.print_exc()
        logger.error('STORE CHANGE : ' , ex.message)

def push_to_mongo(doc):
    try:
        client = MongoClient(MONGO_UPDATE, connect=False)
        db = client.get_database()
        coll_name = mongo_watch["db_name"]+'_changes'
        coll = db[coll_name]
        coll.insert(doc)
    except Exception as ex:
        traceback.print_exc()
        logger.error('PUSH TO MONGO : ' , ex)

if __name__ == '__main__':
    try:
        print(MONGO_ACCESS)
        client = MongoClient(MONGO_ACCESS, connect=False)
        db = client.get_database()
        collections = db.collection_names()
        ##Removing vid_* for getting tracked. Thousands of threads will be spawned otherwise.
        collections = [coll for coll in collections if 'vid_' not in coll]
        print(collections)
        results = Parallel(n_jobs=len(collections), backend="threading")(map(delayed(poll_stream), collections))
        logger.info(results)
    except Exception as ex:
        logger.error('MAIN : ' , ex)
