from pprint import pprint
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from bson import *
import datetime
import time
from pymongo import MongoClient
import pandas as pd
import configparser


config = configparser.ConfigParser()
config.read('/home/swetha/config.ini')
today = datetime.date.today()-datetime.timedelta(days=1)
epoch = int(time.mktime(time.strptime(str(today), "%Y-%m-%d")))*1000

print(epoch)

uri = config['mongo']['mongouri_read']
client = MongoClient(uri)
db = client.get_database()
sketch_coll = db.sketchInfo

final_list = []
print(sketch_coll.find({"created_on":{"$gte":epoch}}))
for sketch in sketch_coll.find({"created_on":{"$gte":epoch}}):
#	try:
		sketch_id=sketch.get('sketch_id','')
		char_id = sketch.get('char_id','')
		title = sketch.get('title','').get('en','')
		subtitle = sketch.get('subtitle','').get('en','')
		print(sketch_id,char_id,title,subtitle)
		final_list.append([sketch_id,char_id,title,subtitle])
#	except Exception as e:
#		print('Exception:'+str(e))


#print(cou)
df = pd.DataFrame(data=final_list)
print(final_list)
df.columns = ["sketch_id",'char_id','title','sub_title']
df = df.loc[df.title != 'Uncredited']
df['image_link'] = "https://assets.charmboard.com/images/im/ca/"+df.char_id.astype(str)+"/a.jpg"
df.to_csv('/home/swetha/daily_cast.csv',index=None)
print(df.head(),df.shape)

sender = 'report@charmboard.com'
recipient = ['ayushi.agrawal@charmboard.com','jp.singh@charmboard.com','shruti.dhawan@charmboard.com','gbs.bindra@charmboard.com','srinivas.shanmugam@charmboard.com', 'yuvaraj.sekhar@charmboard.com']
#recipient = ['surya.gunti@charmboard.com']
cc_recipient =  [ 'surya.gunti@charmboard.com', 'swetha.srinivasan@charmboard.com']
#cc_recipient = ['']

# Create the root message and fill in the from, to, and subject headers
msgRoot = MIMEMultipart('related')
msgRoot['Subject'] = 'Daily Cast Report'
msgRoot['From'] = sender
msgRoot['To'] = ', '.join(recipient)
msgRoot['Cc'] = ', '.join(cc_recipient)
msgRoot.preamble = 'This is a multi-part message in MIME format.'

# Encapsulate the plain and HTML versions of the message body in an
# 'alternative' part, so message agents can decide which they want to display.
msgAlternative = MIMEMultipart('alternative')
msgRoot.attach(msgAlternative)

msgText = 'Hi,<br>Please find below the Cast Info on '+str(today.day)+' '+calendar.month_name[today.month]+'<br>'+\
'<br>Total Cast: '+str(df.sketch_id.nunique())+\
'<br><br>Thanks, <br>Surya.'

msg = MIMEText(msgText,'html')
#msgAlternative.attach(msgText)
msgAlternative.attach(msg)
# This example assumes the image is in the current directory
#msgRoot.attach(MIMEText(file("/var/app/cbtouchan/swetha/daily_card/"+s[:-4]+".pdf").read()))
directory = "/home/swetha/daily_cast.csv"
with open(directory, "rb") as opened:
    openedfile = opened.read()
attachedfile = MIMEApplication(openedfile, _subtype = "csv", )
attachedfile.add_header('content-disposition', 'attachment', filename = "cast_"+str(today)+".csv")
msgRoot.attach(attachedfile)


import smtplib
recipient.extend(cc_recipient)
#Create server object with SSL option
server = smtplib.SMTP('smtp.mailgun.org', 587)
#Perform operations via server
server.login('postmaster@gitlab.charmd.me', '0ab5afc8b23cd160bc1f6f5cfc4acb21-2b4c5a6c-1a9efa3a')
server.sendmail(sender, recipient, msgRoot.as_string())
server.quit()
