from pprint import pprint
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from bson import *
import datetime
import time
from pymongo import MongoClient
import pandas as pd
import configparser


config = configparser.ConfigParser()
config.read('/home/swetha/config.ini')
today = datetime.date.today()-datetime.timedelta(days=1)
epoch = int(time.mktime(time.strptime(str(today), "%Y-%m-%d"))+int(4.5*60*60))*1000
epoch1 = epoch+(24*60*60)*1000
print(epoch,epoch1)


#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
uri = config['mongo']['mongouri_read']
client = MongoClient(uri)
db = client.get_database()
#charms_coll = db.charm
cards_coll = db.card
#vid_coll = db.video
#for i in cards_coll.find( {"$and":[{"created_on":{"$gt":epoch}},{"created_on":{"$lt":epoch1}}]}):
final_list = []
print(cards_coll.find({"created_on":{"$gte":epoch,"$lte":epoch1}}))
for card in cards_coll.find({"created_on":{"$gte":epoch,"$lte":epoch1}}):
	try:
		card_id = card['card_id']
		product_url = card['product_url']
		charm_id = card['associated_charms'][0]['charm_id']
		if 'utm_source' in product_url:
			try:
				utm_source = product_url.split('utm_source=')[1].split('&')[0]
			except IndexError:
				utm_source = ''
			if 'harmboard' not in utm_source:
				final_list.append([charm_id, card_id, utm_source, product_url])
	except KeyError:
		print(card)

df = pd.DataFrame(data=final_list)
#print(final_list)
df.columns = ["charm_id","card_id","utm_source", "product_url"]
#df=df.loc[df.video_type == 'Episode']
df.to_csv('/home/swetha/cards_operations/cards_utm.csv',index=None)
print(df.head())

sender = 'report@charmboard.com'
recipient = ['amit.bajaj@charmboard.com', 'gbs.bindra@charmboard.com','srinivas.shanmugam@charmboard.com', 'yuvaraj.sekhar@charmboard.com']
#recipient = ['swetha.srinivasan@charmboard.com']
cc_recipient =  [ 'swetha.srinivasan@charmboard.com', 'surya.gunti@charmboard.com']
#cc_recipient = ['']

# Create the root message and fill in the from, to, and subject headers
msgRoot = MIMEMultipart('related')
msgRoot['Subject'] = 'Cards with UTM Report'
msgRoot['From'] = sender
msgRoot['To'] = ', '.join(recipient)
msgRoot['Cc'] = ', '.join(cc_recipient)
msgRoot.preamble = 'This is a multi-part message in MIME format.'

# Encapsulate the plain and HTML versions of the message body in an
# 'alternative' part, so message agents can decide which they want to display.
msgAlternative = MIMEMultipart('alternative')
msgRoot.attach(msgAlternative)

msgText = 'Hi,<br>Please find below Cards with non Charmboard UTM codes for '+str(today.day)+' '+calendar.month_name[today.month]+' 10AM to today 10 AM.<br><br>'+\
'<br>Total Charms: '+str(df.charm_id.nunique())+\
'<br>Total Cards: '+str(df.card_id.nunique())+\
'<br><br>Thanks, <br>Swetha.'

msg = MIMEText(msgText,'html')
#msgAlternative.attach(msgText)
msgAlternative.attach(msg)
# This example assumes the image is in the current directory
#msgRoot.attach(MIMEText(file("/var/app/cbtouchan/swetha/daily_card/"+s[:-4]+".pdf").read()))
directory = "/home/swetha/cards_operations/cards_utm.csv"
with open(directory, "rb") as opened:
    openedfile = opened.read()
attachedfile = MIMEApplication(openedfile, _subtype = "csv", )
attachedfile.add_header('content-disposition', 'attachment', filename = "cards_utm_"+str(today)+".csv")
msgRoot.attach(attachedfile)


import smtplib
recipient.extend(cc_recipient)
#Create server object with SSL option
server = smtplib.SMTP('smtp.mailgun.org', 587)
#Perform operations via server
server.login('postmaster@gitlab.charmd.me', '0ab5afc8b23cd160bc1f6f5cfc4acb21-2b4c5a6c-1a9efa3a')
server.sendmail(sender, recipient, msgRoot.as_string())
server.quit()

