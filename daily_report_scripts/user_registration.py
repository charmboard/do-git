import datetime
import time
import calendar
from pymongo import MongoClient
import pandas as pd
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
import configparser


config = configparser.ConfigParser()
config.read('/home/swetha/config.ini')
today = datetime.date.today()-datetime.timedelta(days=1)
epoch = int(time.mktime(time.strptime(str(today), "%Y-%m-%d"))-int(5.5*60*60))*1000
epoch1 = epoch+(24*60*60*1000)
print(epoch,epoch1)
#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
uri = config['mongo']['mongouri_read']
client = MongoClient(uri)
db = client.get_database()
user_coll = db.users

#for i in cards_coll.find( {"$and":[{"created_on":{"$gt":epoch}},{"created_on":{"$lt":epoch1}}]}):
final_list = []

for user in user_coll.find({"created_date":{"$gte":epoch,"$lt":epoch1}}):
		date = user.get('created_date')
		datetime = (time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime((date+(5.5*60*60*1000))/1000)))
		name = user.get('name')
		provider = user.get('provider')
		final_list.append([datetime,name,provider])
df = pd.DataFrame(final_list,columns=['datetime','name','provider'])
df.to_csv('/home/swetha/users.csv',index=None)


sender = 'report@charmboard.com'
recipient = ['ayushi.agrawal@charmboard.com', 'gbs.bindra@charmboard.com', 'sanjana.gupta@charmboard.com','srinivas.shanmugam@charmboard.com', 'yuvaraj.sekhar@charmboard.com']
#recipient = ['surya.gunti@charmboard.com']
cc_recipient =  [ 'surya.gunti@charmboard.com', 'swetha.srinivasan@charmboard.com']
#cc_recipient = ['']

# Create the root message and fill in the from, to, and subject headers
msgRoot = MIMEMultipart('related')
msgRoot['Subject'] = 'Daily User Registration Report'
msgRoot['From'] = sender
msgRoot['To'] = ', '.join(recipient)
msgRoot['Cc'] = ', '.join(cc_recipient)
msgRoot.preamble = 'This is a multi-part message in MIME format.'

# Encapsulate the plain and HTML versions of the message body in an
# 'alternative' part, so message agents can decide which they want to display.
msgAlternative = MIMEMultipart('alternative')
msgRoot.attach(msgAlternative)

msgText = 'Hi,<br>Please find below User Registration Statistics for '+str(today.day)+' '+calendar.month_name[today.month]+\
'.<br><br>Total Users: '+str(df.shape[0])+\
'<br><br>Thanks, <br>Swetha.'

msg = MIMEText(msgText,'html')
#msgAlternative.attach(msgText)
msgAlternative.attach(msg)
# This example assumes the image is in the current directory
#msgRoot.attach(MIMEText(file("/var/app/cbtouchan/swetha/daily_card/"+s[:-4]+".pdf").read()))
directory = "/home/swetha/users.csv"
with open(directory, "rb") as opened:
    openedfile = opened.read()
attachedfile = MIMEApplication(openedfile, _subtype = "csv", )
attachedfile.add_header('content-disposition', 'attachment', filename = "users_"+str(today)+".csv")
msgRoot.attach(attachedfile)


import smtplib
recipient.extend(cc_recipient)
#Create server object with SSL option
server = smtplib.SMTP('smtp.mailgun.org', 587)
#Perform operations via server
server.login('postmaster@gitlab.charmd.me', '0ab5afc8b23cd160bc1f6f5cfc4acb21-2b4c5a6c-1a9efa3a')
server.sendmail(sender, recipient, msgRoot.as_string())
server.quit()


