import time
from pymongo import MongoClient
import pymongo.errors
import pprint
import pandas as pd
import urllib
import json
import random
import configparser
from bson.objectid import ObjectId

config = configparser.ConfigParser()
config.read('/home/swetha/config.ini')

def getYoutubeViews(link_id,count):
	views = 0
	avg_views = 0
	try:
		url="https://www.googleapis.com/youtube/v3/videos?part=contentDetails,statistics&id="+link_id+"&key=AIzaSyBWq5F5tOD-YKCWu9jibyHCjCho6pTNTY8"
		response = urllib.request.urlopen(url)
		html = response.read()
		views = json.loads(html.decode("utf-8"))['items'][0]['statistics']['viewCount']
		avg_views = int(views) / int(count)
	except IndexError:
		print(link_id,json.loads(html.decode("utf-8")))
	except:
		print(link_id,'HTTPError')
	return int(views),int(avg_views)

def randomAvgViews(avg):
	avg_new = avg * (random.randrange(70,100,5)*0.01)
	print(avg, int(avg_new))
	return int(avg_new)

uri = config['mongo']['mongouri_write']
#uri = "mongodb://localhost:27017/charmboard_dev"
client = MongoClient(uri)
db = client.get_database()
charms_coll = db.charm
videos_coll = db.video

arr = []

for charm in charms_coll.find():
	try:
		charm_id = charm['charm_id']
		video_url = charm['associated_videos'][0]['video_url'].strip()
		video_id = charm['associated_videos'][0]['video_id']
		if 'yout' in video_url:
			len_url = len(video_url)
			link_id = ''
			if len_url < 35:
				link_id = video_url.split("/")[-1]
			elif '=' in video_url and '&' not in video_url and '#' not in video_url and 'vv' != video_url[-2:]:
				link_id = video_url.split("=")[1]
			elif 'vv' == video_url[-2:] and len_url != 43:
				link_id = video_url.split("=")[1][:-2]
			elif '&' in video_url:
				link_id = video_url.split("=")[1].split('&')[0]
			elif '#' in video_url:
				continue
			arr.append([charm_id,link_id,video_id])
	except:
		continue

data_charm = pd.DataFrame(data=arr,columns=['charm_id','link_id','video_id'])

data_video = data_charm.groupby(['video_id','link_id']).count().reset_index()

data_video.columns = ['video_id','link_id','charm_count']

data_video['avg_views_merge'] = 0

print(data_video.shape[0])

print(data_video.head())

for index,row in data_video.iterrows():
#	print(index,row['link_id'])
	total_views_merge,avg_views_merge =  getYoutubeViews(row['link_id'],row['charm_count'])
	print(index,row['video_id'],row['link_id'],total_views_merge,avg_views_merge)
	data_video.set_value(index,'total_views_merge',total_views_merge)
	data_video.set_value(index,'avg_views_merge',avg_views_merge)
#	if index == 'hXh35CtnSyU':
#		break
	if index%700 == 0:
		print("sleeping")
		time.sleep(10)

data = pd.merge(data_charm,data_video,on=['link_id','video_id'],how='left')
data.avg_views_merge = data.avg_views_merge.fillna(0)
data['avg_views'] = list(map(randomAvgViews,data.avg_views_merge))
data.drop(['charm_count','link_id','avg_views_merge'],axis=1,inplace=True)
print(data.shape)

data_video.total_views_merge = data.total_views_merge.fillna(0)
data_video.drop(['charm_count','link_id','avg_views_merge'],axis=1,inplace=True)
print(data_video.shape)

if data.avg_views.sum() == 0:
	print("Rerun script - couldn't fetch views")
print("updating videos <<<<")
for index,row in data_video.iterrows():
	if row['total_views_merge'] > 0:
		try:
			video_id = row['video_id']
			result = videos_coll.update_one({"video_id": video_id},{ "$set": { "video_global_info.views": int(row['total_views_merge'])  } }, upsert=False)
			print(video_id,result)
		except TypeError:
			print("Type Error : check datatype of video_id")
			continue
		except pymongo.errors.WriteError:
			print("validationError video", row)
			continue
print("updating charms <<<<")
for index,row in data.iterrows():
	if row['avg_views'] > 0:
		try:
			charm_id = int(row['charm_id'])
			result = charms_coll.update_one({"charm_id": charm_id},{ "$set": { "ext_likes" :  [ { "youtube": int(row['avg_views']) } ] } }, upsert=False)
			print(charm_id,result)
		except TypeError:
			print("Type Error : check datatype of charm_id")
			continue
		except pymongo.errors.WriteError:
			print("validationError charm", row)
			continue
