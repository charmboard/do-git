from pprint import pprint
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from bson import *
import datetime
import time
from pymongo import MongoClient
import pandas as pd
import configparser


config = configparser.ConfigParser()
config.read('/home/swetha/config.ini')
today = datetime.date.today()-datetime.timedelta(days=1)
epoch = int(time.mktime(time.strptime(str(today), "%Y-%m-%d"))+int(12.5*60*60))
epoch1 = epoch+(16*60*60)
print(epoch,epoch1)
#uri = "mongodb://tagosprod:tagosprod#132@ds119295-a0.mlab.com:19295/charmboard"
uri = config['mongo']['mongouri_read']
client = MongoClient(uri)
db = client.get_database()
charms_coll = db.charm
cards_coll = db.card
vid_coll = db.video
#for i in cards_coll.find( {"$and":[{"created_on":{"$gt":epoch}},{"created_on":{"$lt":epoch1}}]}):
final_list = []
print(vid_coll.find({"created_on":{"$gte":epoch,"$lte":epoch1}}))
for video in vid_coll.find({"created_on":{"$gte":epoch,"$lte":epoch1}}):
#	try:
		charm_list=video.get('video_charms')
		video_id=video.get('video_id')
		video_type=video.get('video_type')
		#print(video_id,charm_list)
		for charm_id in charm_list:
			obj=charms_coll.find({"charm_id":charm_id})
			#print("charm: ",charm_id)
			try:
				card_list = obj[0]['content_image_cards']
				#print("charm: ",charm_id)
				for card in card_list:
					category = card['cb_category']
					sub_category = card['sub_category']
					subsub_category = card['subsub_category']
					card_ids=str(card['card_id']).split(',')
					#print(card['card_id'])
					for card_id in card_ids:
						#print("card: ",card_id)
						card_obj=cards_coll.find_one({"card_id":int(card_id)})
						try:
							product_url=card_obj.get('product_url') 
						except AttributeError:
							product_url = ''
						if product_url:
                        				try:
	                                			if 'www.' in product_url:
        	                                			product_url = product_url.split('www.')[1].split('.')[0]
                	                			elif '://' in product_url:
                        	                			product_url = product_url.split('://')[1].split('.')[0]
                        				except:
                                				print('Exception in Product_url')
						final_list.append([video_id,video_type,charm_id,card_id,product_url,category,sub_category,subsub_category])
			except:
				final_list.append([video_id,video_type,charm_id,"no cards for this charm","","","",""])
#	except Exception as e:
#		print('Exception:'+str(e))


#print(cou)
df = pd.DataFrame(data=final_list)
print(final_list)
df.columns = ["video_id","video_type","charm_id","card_id","domain","category","sub_category","subsub_category"]
df=df.loc[df.video_type == 'Episode']
df.to_csv('/home/swetha/cards_operations/cards_created_mor.csv',index=None)
print(df.head())

sender = 'report@charmboard.com'
recipient = ['amit.bajaj@charmboard.com', 'gbs.bindra@charmboard.com','srinivas.shanmugam@charmboard.com','yuvaraj.sekhar@charmboard.com']
#recipient = ['surya.gunti@charmboard.com']
cc_recipient =  [ 'surya.gunti@charmboard.com', 'swetha.srinivasan@charmboard.com']
#cc_recipient = ['']

# Create the root message and fill in the from, to, and subject headers
msgRoot = MIMEMultipart('related')
msgRoot['Subject'] = 'Serial Operations Report'
msgRoot['From'] = sender
msgRoot['To'] = ', '.join(recipient)
msgRoot['Cc'] = ', '.join(cc_recipient)
msgRoot.preamble = 'This is a multi-part message in MIME format.'

# Encapsulate the plain and HTML versions of the message body in an
# 'alternative' part, so message agents can decide which they want to display.
msgAlternative = MIMEMultipart('alternative')
msgRoot.attach(msgAlternative)

msgText = 'Hi,<br>Please find below Serial Operations Statistics for '+str(today.day)+' '+calendar.month_name[today.month]+' 6PM to 10AM.<br><br>'+\
'Total Videos: '+str(df.video_id.nunique())+\
'<br>Total Charms: '+str(df.charm_id.nunique())+\
'<br>Total Cards: '+str(df.card_id.nunique())+\
'<br>Total Domains: '+str(df.domain.nunique())+\
'<br><br>Thanks, <br>Swetha.'

msg = MIMEText(msgText,'html')
#msgAlternative.attach(msgText)
msgAlternative.attach(msg)
# This example assumes the image is in the current directory
#msgRoot.attach(MIMEText(file("/var/app/cbtouchan/swetha/daily_card/"+s[:-4]+".pdf").read()))
directory = "/home/swetha/cards_operations/cards_created_mor.csv"
with open(directory, "rb") as opened:
    openedfile = opened.read()
attachedfile = MIMEApplication(openedfile, _subtype = "csv", )
attachedfile.add_header('content-disposition', 'attachment', filename = "cards_6PM-10AM_"+str(today)+".csv")
msgRoot.attach(attachedfile)


import smtplib
recipient.extend(cc_recipient)
#Create server object with SSL option
server = smtplib.SMTP('smtp.mailgun.org', 587)
#Perform operations via server
server.login('postmaster@gitlab.charmd.me', '0ab5afc8b23cd160bc1f6f5cfc4acb21-2b4c5a6c-1a9efa3a')
server.sendmail(sender, recipient, msgRoot.as_string())
server.quit()
